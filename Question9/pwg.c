#define _XOPEN_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <string.h>
#include <crypt.h>
#define SALT "ABCD"

int main()
{

    //Original password file
    FILE *passwords;

    //Rewritten password file
    FILE *new_passwords;

    passwords = fopen("/home/administrateur/passwords", "r");
    new_passwords = fopen("/home/administrateur/.pwg_temp_passwords", "a");

    

    //Current user creditentials
    char user_password[100];
    int user_id = getuid();

    //Creditentials stored in file
    char file_password[100];
    int file_id;

    int auth = 0;
    int user_found = 0;
    if (passwords == NULL)
    {
        printf("Unable to open file, creating one...\n");
        printf("Please enter your password\n");
        scanf("%s", user_password);
        fprintf(new_passwords, "%d %s\n", user_id, crypt(user_password, SALT));
        fclose(new_passwords);
        rename("/home/administrateur/.pwg_temp_passwords", "/home/administrateur/passwords");
        return 0;
    }

    //Scan file
    while (!feof(passwords) && fscanf(passwords, "%d %s\n", &file_id, file_password))
    {
        //If user ID matches an entry in file
        if (file_id == user_id && auth == 0)
        {
            user_found = 1;
            //Authentification
            printf("Please enter your password");
            scanf("%s", user_password);

            //Password matches, updating password
            if (strcmp( crypt(user_password, SALT) , file_password) == 0)
            {
                //Updating password
                printf("Authentificated, please provide a new password\n");
                char new_password[100];
                scanf("%s", new_password);

                //Writting in new file
                fprintf(new_passwords, "%d %s\n", user_id, crypt(new_password, SALT));
                auth = 1;
            }
            else{
                //Auth failed, writting previous password instead.
                printf("Authentification failed");
                fprintf(new_passwords, "%d %s\n", user_id, file_password);
            }
        }
        else{
            fprintf(new_passwords, "%d %s\n", user_id, file_password);
        }
    }

    if(user_found == 0){
        printf("User does not exist, please enter a new password\n");
        scanf("%s", user_password);
        fprintf(new_passwords, "%d %s\n", user_id, crypt(user_password, SALT));
    }

    //Closing files
    fclose(passwords);
    fclose(new_passwords);

    //Replacing previous file
    remove("/home/administrateur/passwords");
    rename("/home/administrateur/.pwg_temp_passwords", "/home/administrateur/passwords");
   
}