
groupadd groupe_a
groupadd groupe_b


useradd lambda_a -g groupe_a 
useradd lambda_b -g groupe_b

useradd admin_g

sudo su admin_g

mkdir dir_a
mkdir dir_b
mkdir dir_c

sudo chgrp groupe_a dir_a
sudo chgrp groupe_b dir_b

#must change primary group
sudo usermod -g group_a lambda_b
sudo usermod -g group_b lambda_b

#Admin can read write execute
#same group can read write execute
#others can't do anything
#drwxrwx---
sudo chmod 770 dir_a 
#drwxrwx---
sudo chmod 770 dir_b 
#admin and group c can read write execute
#other can read and execute
#drwxrwxr-x
sudo chmod 775 dir_c 

sudo chmod +t dir_c
sudo chmod +t dir_a
sudo chmod +t dir_b

sudo chown admin_g:group_a dir_a
sudo chown admin_g:group_b dir_b
sudo chown admin_g:group_c dir_c