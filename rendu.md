# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: El Abbassi Ilias ilias.elabbassi.etu@univ-lille.fr

- Nom, Prénom, email: Jehl Théo theo.jehl.etu@univ-lille.fr

## Question 1

Oui, le processus peut écrir sur le fichier titi.txt.<br>
ce derniere à les droits ouvert pour l'écrite et la lecture, au groupe ubuntu.<br>
Et comme l'ulisateur toto est dans le gorupe ubuntu, ce dernier peut lancer un processus qui lis dans le fichier titi.txt.<br>
Car lorsqu'un utilisateur lance un programme, ce dernier hérite des droits du de l'utilisateur.

## Question 2

Le caractére x pour un répértoire, signifie qu'on peut y accéder.<br>
<br>
Si nous retirons le droit x du groupe ubuntu, toto ne peux plus y accéder.<br>
Car toto appartiens au groupe ubuntu, mais le groupe ubuntu na pas les droits d'éxécution.<br>
<br>
Lorsque nous créons un fichier dans mydir, toto peux lister les elements de mydir, en utilisant ls.<br>
Cependant il ne peux pas voir toutes les informations (seulement le nom du fichier est visible). <br>
<br>
Ceci est du au fait que le groupe a des droits en lecture.<br>


## Question 3

Les valeurs sont :<br>
```
RUID: 1001
RGID: 1000
EUID: 1001
EGID: 1000
```
<br>
Le processus n'arrive pas a ouvrir le fichier car toto, na pas accés au répertoire mydir (qui contient data.txt).<br>
<br>
si nous ajoutons : chmod u+s main<br>

Le processus lancer par toto peut s'éxécuter sans avoir de probleme de droit.<br>
Car le processus hérite des droits du groupe ubuntu.<br>
<br>
```
RUID: 1001
RGID: 1000
EUID: 1000
EGID: 1000
```
## Question 4

```
EUID: 1OO1
EGID: 1000
```

EGID est a 1000, car nous avons fais en sorte que les processus qui exécute suid.py es les droits d'ubuntu.


## Question 5

CHFN, sert a changer les informations d'un utilisateur. Les informations sont storé dans /etc/passwd.
```
$ ls -al /usr/bin/chfn
```
```
-rwsr-xr-x 1 root root 76496 Jan 25  2018 /usr/bin/chfn
```
Le processus appartiens au groupe root, et lisible et éxécutable par tous le monde.
le **s** dans rw**s**, est le bit de setuid, qui permet de dire à l'OS d'éxécuter le program with le userid du propiétaire du fichier.

```
$ cat /etc/passwd
```
avant le chfn sur toto:
```
toto:x:1001:1000::/home/toto:/bin/bash
```
apres le chfn sur toto:

```
toto:x:1001:1000:,123,456,789:/home/toto:/bin/bash
```
Les informations ont bien été mises à jour.

## Question 6

Les mots de passe sont stocké dans **/etc/shadow**. Il faut bien évidement les permissions root pour pouvoir y accéder.

## Question 7

création des groupes et users:
```
groupadd groupe_a
groupadd groupe_b

useradd lambda_a -g groupe_a
useradd lambda_b -g groupe_b
```

voir fichier question7.bash

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








