#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(){
    printf("RUID:%d\n", getuid());
    printf("RGID:%d\n", getgid());
    printf("EUID:%d\n", geteuid());
    printf("EGID:%d\n", getegid());

    FILE *f;

    f = fopen("/home/ubuntu/mydir/data.txt", "r");
    if(f == NULL){
        printf("Erreur d'ouverture\n");
        return 1;
    }
    else{
        printf("Fichier ouvert\n");
    }

    char ch;
    while((ch = fgetc(f)) != EOF){
        printf("%c", ch);
    }

    return 0;
}
