#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <unistd.h>
#include<pwd.h>
#include<grp.h>
#include"check_pass.h"
#include<errno.h>



int main(int argc, char * argv[]){
    if(argc < 2){
        printf("No file passed\n");
        return 1;
    }

    uid_t userID = getuid();
    gid_t groupID = getgid();
    
    printf("%d %d\n", userID, groupID);

    FILE * file = fopen(argv[1], "r");

    struct stat filestats;

    struct group * groupinfo;

    if(file != NULL){   

        stat(argv[1], &filestats);
        groupinfo = getgrgid(filestats.st_gid);

        //printf("group id %d %d",groupinfo->gr_gid , filestats.st_gid);

        if(groupID != groupinfo->gr_gid ){
            printf("Looks like you don't belong to the same group as the provided file, aborting\n");
            return 1;
        }
        else{
            printf("It belongs to you!");
            fflush(stdout);
        }
    }
    else{
        printf("Failed to open file\n");
        return 1;
    }

    if(!check_pass()){
        return 1;
    }

    if(remove(argv[1]) == 0){
        printf("File deleted successfully");
        return 0;
    }
    else{
        printf("Failed to delete file");
        return 1;
    }

}