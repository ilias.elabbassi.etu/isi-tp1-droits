#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <unistd.h>
#include<pwd.h>
#include<grp.h>
#include"check_pass.h"

#include <crypt.h>
#define SALT "ABCD"

int check_pass(){
    FILE * passwords;

    passwords = fopen("/home/administrateur/passwords", "r");

    if(passwords == NULL){
        printf("Unable to open login file");
        return 1;
    }

    char user_password[100];
    int user_id = getuid();

    char file_password[100];
    int file_id;

    while(!feof(passwords) && fscanf(passwords, "%d %s", &file_id, file_password)){
        if(file_id == user_id){
            printf("Please enter your password\n");
            fflush(stdout);

            scanf("%s", user_password);

            if(strcmp( crypt(user_password, SALT) , file_password) == 0){
                printf("Authentificated\n");
                return 1;
            }
            else{
                printf("Authentification failed");
                return 0;
            }
        }
    }
    if(feof(passwords)){
        printf("User not registered in database");
        return 0;
    }
    return 0;
}

